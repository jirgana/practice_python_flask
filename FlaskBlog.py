from flask import Flask, render_template, url_for 

app = Flask(__name__)

posts = [
    {
        'author' : '김용진', 
        'title' : '글1',
        'content' : '첫글내용',
        'date_posted' : '2021/06'
    },
    {
        'author' : '이진주', 
        'title' : '글2',
        'content' : '두번째 글내용',
        'date_posted' : '2021/07'
    }

]

@app.route("/home")
def home():
    return render_template('home.html', posts = posts)

@app.route("/about")
def about():
    # return "<h1>About Page</h1>"
    return render_template('about.html', title = 'About')

if __name__ == '__main__' :
    app.run(debug=True)